# Shell

> The base container for all website on [montreal.ca](https://beta.montreal.ca).

It provides a default User Experience for all services provided by the City. With it, independant applications can be combined transparently into a single website such that the user never knows they are navigating from one application to another.

## Development

### Technology

This project is built using TypeScript and Express, while all front-end assets are built using Webpack. The Shell has very few pages in itself, and mostly delegates the responsability of showing content to external applications. Whenever a page is requested, a call is made to an external service based on the URL and the received HTML is then parsed and inserted into the final response. The Shell adds a couple of parts on top of the received content, mainly the header, footer and menu.

### Requirements

* [node.js](https://nodejs.org/en/)
* [yarn](https://yarnpkg.com/lang/en/)

### Recommended

If you're using VS Code, you can install the Prettier and TSLint extensions as this project uses both for linting purposes. Make sure that all of the files pass the lint check before committing any changes, otherwise the build will fail and you will have to fix it. The rules come from our standard config [lint-config-villemontreal](https://bitbucket.org/villemontreal/lint-config-villemontreal).

### Commands

```sh
# Install dependencies
yarn

# Run the local version
yarn serve

# Run the linter
yarn lint

# Fix all auto-fixable problems in the current project
yarn lint-fix
```

### Points of attention

Since content coming from external applications are inserted into the Shell's page, it's very important to prevent any possible clash between the two. See the following examples of things to remember when developing in the context of the Shell:

* Prefix all CSS classes with `shell-`
* Prefix all front-end assets with `/shell/`
* Avoid using CSS tag selector to target all elements on a page
* Don't load huge CSS/JS libraries, rather let each application load whatever they need

### JavaScript bridge API

There are several use case where the external application must communicate with the Shell to modify its behavior. An example of this is to be able to set the URL of the "Change language" button in the menu, or redirecting to a 404 while in a SPA. To accomplish this, the Shell creates a special JavaScript object in the page that the application can then call.

```TypeScript
window._vdm.sendMessage(message: string, data: any);
```

This method then dispatches to the appropriate implementation depending of the message received. This method must remain backward-compatible, otherwise applications depending on it could end up broken. The clients shouldn't call this method directly, but rather use the correctly typed [helper library](https://bitbucket.org/villemontreal/shell-portal-client-lib) that delegates to this method if it exists. The library also provides a stub that clients can use in development if they want to run outside of the Shell. As such, when implementing a new message, a new corresponding method must also be added to the helper library.

## Integration

When integrating an external application with the Shell for the first time, an URL prefix must first be decided for the application. This prefix will be used by the Shell to route all incoming requests to your application. The only application without a prefix is the Portal, and as such any unrecognized URL is sent to it. If your application is localized, you must provide a prefix for each supported language. The format of the URL can't be changed, as the Shell uses it to determine the user's language when showing the header.

```sh
# For a tax application
fr => /taxe/*
en => /en/tax/*
```

To create your application, it's recommended to use the [Angular SPA generator](https://bitbucket.org/villemontreal/generator-mtl-angular-spa). The generator has been tested for compatibility in the Shell and will work out of the box with it. If your application has specific non-default requirements (such as not showing the header), it can still be integrated but will require additional work.

The Shell also makes a certain list of methods available to the external applications through the browser native's JavaScript API, which your application can call at runtime to modify the Shell's behavior. Such methods include error reporting, localization helper and more. Please refer to the [helper library](https://bitbucket.org/villemontreal/shell-portal-client-lib) for more information on available methods.

## List of services

Here is the current list of services integrated in the Shell.

* [Portal](https://bitbucket.org/villemontreal/portail-internet-web)
