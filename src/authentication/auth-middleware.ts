import { NextFunction, Request, Response } from 'express';
import { config } from '../utils/config';

export function authMiddleware(req: Request, res: Response, next: NextFunction) {
  res.locals.clientSideData = res.locals.clientSideData || {};
  res.locals.clientSideData.gluu = config.gluu;

  res.locals.isAuthenticated = !!req.cookies.access_token;
  next();
}
