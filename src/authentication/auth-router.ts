import { NextFunction, Request, Response, Router } from 'express';

export const authRouter = Router();

authRouter.get('/fr/authoriser', authenticationCallback);
authRouter.get('/en/authorize', authenticationCallback);
authRouter.get('/fr/deconnexion', authenticationCallback);
authRouter.get('/en/logout', authenticationCallback);
authRouter.get('/silent-refresh.html', authenticationCallback);

function authenticationCallback(req: Request, res: Response, next: NextFunction) {
  // Show an empty page since all the logic is done client-side
  return res.render('ExternalPage');
}
