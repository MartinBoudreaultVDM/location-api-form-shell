import { Application } from 'express';
import { authMiddleware } from './auth-middleware';
import { authRouter } from './auth-router';

export function initializeAuthentication(app: Application) {
  app.use(authMiddleware);
  app.use(authRouter);
}
