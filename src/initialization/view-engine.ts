// tslint:disable:no-var-requires
import { Application } from 'express';
import * as path from 'path';
const expressReactViews = require('express-react-views');

export function viewEngine(app: Application) {
  app.set('views', path.join(__dirname, '..', 'views', 'pages'));

  const viewEngineExtension = getViewEngineExtension();
  app.set('view engine', viewEngineExtension);
  app.engine(viewEngineExtension, expressReactViews.createEngine());
}

function getViewEngineExtension(): string {
  const rootFolder = path.join(__dirname, '..');

  if (path.basename(rootFolder) === 'src') {
    return 'tsx';
  }

  return 'js';
}
