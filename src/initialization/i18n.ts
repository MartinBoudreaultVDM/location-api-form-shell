import { Application, NextFunction, Request, Response } from 'express';
import * as i18next from 'i18next';
import * as i18nextMiddleware from 'i18next-express-middleware';
import * as FileSystemBackend from 'i18next-node-fs-backend';
import * as path from 'path';
import { logger } from '../utils';

export function initializeI18n(app: Application) {
  i18next
    .use(i18nextMiddleware.LanguageDetector)
    .use(FileSystemBackend)
    .init({
      backend: {
        loadPath: path.join(__dirname, '..', '..', 'locales', '{{lng}}', '{{ns}}.json')
      },
      defaultNS: 'common',
      detection: {
        lookupFromPathIndex: 0,
        order: ['path']
      },
      fallbackLng: 'fr',
      missingKeyHandler: logMissingKey,
      ns: ['common', 'header', 'footer', '404'],
      preload: ['fr', 'en'],
      saveMissing: true,
      whitelist: ['fr', 'en']
    });

  app.use(i18nextMiddleware.handle(i18next));
  app.use(exposeTranslationFunctionToLocals);
}

function logMissingKey(language: string, namespace: string, key: string) {
  logger.warn(`Missing i18n key: [${language}]${namespace}:${key}`);
}

function exposeTranslationFunctionToLocals(req: Request, res: Response, next: NextFunction) {
  const i18n = (req as any).i18n;
  res.locals.t = i18n.t.bind(i18n);

  res.locals.clientSideData = res.locals.clientSideData || {};
  res.locals.clientSideData.language = i18n.language;

  next();
}
