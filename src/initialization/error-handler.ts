import { Application, NextFunction, Request, Response } from 'express';
import { logger } from '../utils';

export function registerErrorHandler(app: Application) {
  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err: any = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    logger.error(err);
    const statusCode = getErrorStatusCode(err);

    if (statusCode === 404) {
      res.status(404).render('NotFoundPage');
    } else {
      // set locals, only providing error in development
      const props = req.app.get('env') === 'development' ? { error: err } : undefined;

      res.status(500).render('UnhandledErrorPage', props);
    }
  });
}

function getErrorStatusCode(err: any) {
  if (err.status) {
    return err.status;
  }

  if (err.response && err.response.status) {
    return err.response.status;
  }

  return 500;
}
