import * as express from 'express';
import * as path from 'path';
import * as favicon from 'serve-favicon';

const oneYear = 1000 * 60 * 60 * 24 * 365;

export function servePublic(app: express.Application) {
  if (process.env.LOCAL === 'true') {
    devServePublic(app);
  } else {
    prodServePublic(app);
  }
  const autocompletePath = path.join(__dirname, '../..', 'autocomplete');
  app.use('/suggester', express.static(autocompletePath));
}

function devServePublic(app: express.Application) {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackConfig = require('../../webpack.dev');
  app.use(
    '/shell',
    webpackDevMiddleware(webpack(webpackConfig), {
      stats: {
        colors: true
      }
    })
  );
}

function prodServePublic(app: express.Application) {
  const publicPath = path.join(__dirname, '..', 'public');
  app.use('/shell', favicon(path.join(publicPath, 'favicon.png')));
  app.use('/shell', express.static(publicPath, { maxAge: oneYear }));
}
