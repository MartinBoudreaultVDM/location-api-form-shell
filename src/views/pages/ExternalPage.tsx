import * as React from 'react';
import { Layout } from '../layout';
import { ILocalsProps } from './locals';

interface IIndexPageProps {
  head: string;
  body: string;
  bodyAttributes: { [name: string]: string };
}

export default class IndexPage extends React.PureComponent<IIndexPageProps & ILocalsProps> {
  public render() {
    return (
      <Layout
        t={this.props.t}
        head={this.props.head}
        bodyAttributes={this.props.bodyAttributes}
        isAuthenticated={this.props.isAuthenticated}
        clientSideData={this.props.clientSideData}
      >
        <div dangerouslySetInnerHTML={{ __html: this.props.body }} />
      </Layout>
    );
  }
}
