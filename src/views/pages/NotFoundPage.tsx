import * as React from 'react';
import { InlineSvg } from '../InlineSvg';
import { Layout } from '../layout';
import { ILocalsProps } from './locals';

export default class NotFoundPage extends React.PureComponent<ILocalsProps> {
  public render() {
    return (
      <Layout t={this.props.t} isAuthenticated={this.props.isAuthenticated} clientSideData={this.props.clientSideData}>
        <div className="shell-page-error">
          <h1>{this.props.t!('404:header')}</h1>
          <div className="shell-divider" />
          <div className="h3">{this.props.t!('404:subHeader')}</div>
          <p>{this.props.t!('404:message')}</p>
          <p>
            <a className="shell-regular-link" href={this.props.t!('404:goBackToHomePageUrl')}>
              <span className="shell-underline">{this.props.t!('404:goBackToHomePage')}</span>
              <InlineSvg
                src="@villemontreal/font-icon/svg-files/063-fleche-droite.svg"
                className="shell-vdm-063-fleche-droite"
              />
            </a>
          </p>
        </div>
      </Layout>
    );
  }
}
