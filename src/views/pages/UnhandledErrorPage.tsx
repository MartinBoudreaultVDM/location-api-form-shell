import * as React from 'react';
import { Layout } from '../layout';
import { ILocalsProps } from './locals';

interface IErrorPageProps {
  error?: {
    status: number;
    stack: string;
  };
}

export default class ErrorPage extends React.PureComponent<IErrorPageProps & ILocalsProps> {
  public render() {
    return (
      <Layout t={this.props.t} isAuthenticated={this.props.isAuthenticated} clientSideData={this.props.clientSideData}>
        <div className="shell-page-error">
          <h1>Site en maintenance / Site under maintenance</h1>
          <div className="shell-divider" />
          <h2>
            Désolé, le site est actuellement en maintenance. On se revoit plus tard.
            <br />
            <br />
            We're sorry - the site is currently under maintenance. We'll see you later.
            <br />
            <br />
          </h2>
          {this.props.error ? (
            <div>
              <div>{this.props.error.status}</div>
              <pre>{this.props.error.stack}</pre>
            </div>
          ) : null}
        </div>
      </Layout>
    );
  }
}
