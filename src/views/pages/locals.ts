import { TranslationFunction } from 'i18next';

export interface ILocalsProps {
  isAuthenticated: boolean;
  clientSideData: { [key: string]: any };
  t: TranslationFunction;
}
