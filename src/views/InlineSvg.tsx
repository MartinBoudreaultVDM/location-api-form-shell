import * as React from 'react';
import { parseString } from 'xml2js';
import { readModuleFileSync } from '../utils';
import { parseStyles } from './parse-styles';

interface IInlineSvgProps {
  src: string;
  className?: string;
}

export class InlineSvg extends React.PureComponent<IInlineSvgProps> {
  public render() {
    const { $: attributes, ...children } = this.getSvgContent();

    return (
      <svg dangerouslySetInnerHTML={{ __html: this.mapChildrenToHtml(children) }} className={this.props.className} {...this.filterAttributes(attributes)} />
    );
  }

  private getSvgContent() {
    const svgContent = readModuleFileSync(this.props.src);
    let xml: any = {};

    parseString(svgContent, { async: false }, (err, result) => {
      if (err) {
        throw new Error(`Couldn't parse file ${this.props.src} as XML: ${err.message}`);
      }

      xml = result;
    });

    if (!xml.svg) {
      throw new Error(`File ${this.props.src} is not a valid SVG`);
    }

    return xml.svg;
  }

  private mapChildrenToHtml(children: any): string {
    return Object.keys(children).reduce((acc, tag) => {
      // Ignore title tag
      if (tag === 'title') {
        return acc;
      }

      return children[tag].reduce((acc2: string, tagInstance: any) => {
        const { $: attributes, ...innerChildren } = tagInstance;

        let seralizedAttributes = '';
        if (attributes) {
          Object.keys(attributes).forEach(attributeKey => {
            seralizedAttributes += ` ${attributeKey}="${attributes[attributeKey]}"`;
          });
        }

        const serializedTag = `<${tag}${seralizedAttributes}>
  ${this.mapChildrenToHtml(innerChildren)}
</${tag}>
`;
        return acc2 + serializedTag;
      }, acc);
    }, '');
  }

  private filterAttributes(attributes: any) {
    // Transform style from a string to a JS object
    if (attributes.style) {
      attributes.style = parseStyles(attributes.style);
    }

    // Remove attributes containing a ':', mostly namespace declaration
    Object.keys(attributes).forEach(key => {
      if (key.includes(':')) {
        delete attributes[key];
      }
    });
    
    return attributes;
  }
}
