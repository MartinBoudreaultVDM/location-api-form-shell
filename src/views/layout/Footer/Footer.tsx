import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { SocialMediaLink } from './SocialMediaLink';

interface IFooterProps {
  t: TranslationFunction;
}

export class Footer extends React.PureComponent<IFooterProps> {
  public render() {
    return (
      <footer className="shell-footer" data-component-type="navigation" data-component-position="bas de page" data-component-name="pied de page">
        <div className="shell-footer-container">
          <div className="shell-footer-left">
            <div>{ this.props.t('footer:copyright') }</div>
            <a href={this.props.t('footer:contactUsUrl')}
              title={this.props.t('footer:contactUsTitle')}
              aria-label={this.props.t('footer:contactUsAria')}>
              { this.props.t('footer:contactUs') }
            </a>
          </div>
          <div className="shell-footer-center">
            <SocialMediaLink title={this.props.t('footer:facebookTitle')} url="https://www.facebook.com/mtlville" icon="101-social-facebook" />
            <SocialMediaLink title={this.props.t('footer:twitterTitle')} url="https://twitter.com/mtl_ville" icon="099-social-twitter" />
            <SocialMediaLink title={this.props.t('footer:youTubeTitle')} url="https://www.youtube.com/user/MTLVille" icon="100-social-youtube" />
            <SocialMediaLink title={this.props.t('footer:linkedInTitle')} url="https://www.linkedin.com/company/449259/" icon="098-social-linkedin" />
          </div>
          <div className="shell-footer-right">
            <a href={this.props.t('footer:privacyPolicyUrl')}
              title={this.props.t('footer:privacyPolicyTitle')}
              aria-label={this.props.t('footer:privacyPolicyAria')}>
              { this.props.t('footer:privacyPolicy') }
            </a>
          </div>
        </div>
      </footer>
    );
  }
}
