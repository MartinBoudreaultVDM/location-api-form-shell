import * as React from 'react';
import { InlineSvg } from '../../InlineSvg';

interface ISocialMediaLinkProps {
  icon: string;
  title: string;
  url: string;
}

export class SocialMediaLink extends React.PureComponent<ISocialMediaLinkProps> {
  public render() {
    return (
      <a href={this.props.url} target="_blank" title={this.props.title} data-link-title={this.props.title}>
        <InlineSvg src={`@villemontreal/font-icon/svg-files/${this.props.icon}.svg`} className="shell-social-media" />
      </a>
    );
  }
}
