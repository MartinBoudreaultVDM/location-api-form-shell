import { TranslationFunction } from 'i18next';
import * as React from 'react';
const { version } = require('../../../package.json');

interface IHeadProps {
  clientSideData: { [key: string]: string };
  t: TranslationFunction;
  title?: string;
  includeTitle?: boolean;
}

const defaultTitle = 'Ville de Montréal';

export class Head extends React.PureComponent<IHeadProps> {
  public render() {
    return [
      this.props.includeTitle ? <title>{this.props.title || defaultTitle}</title> : null,
      <link rel="shortcut icon" type="image/png" href="/shell/favicon.png" />,
      <link href={`/shell/stylesheets/style.css?v=${version}`} rel="stylesheet" />,
      <link
        href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet"
      />,
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />,
      <script
        dangerouslySetInnerHTML={{
          __html: `
  window.shellConfig = JSON.parse('${JSON.stringify(this.props.clientSideData)}');
`
        }}
      />,
      <script src={`/shell/javascripts/bridge.js?v=${version}`} />
    ];
  }
}
