import * as React from 'react';
import { InlineSvg } from '../../InlineSvg';

export class MenuButton extends React.PureComponent {
  public render() {
    return (
      <button className="shell-menu-button" id="shell-menu-button">
        <span className="sr-only">Ouvrir la barre de menu</span>
        <InlineSvg src="@villemontreal/font-icon/svg-files/060-menu.svg" />
      </button>
    );
  }
}
