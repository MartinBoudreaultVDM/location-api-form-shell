import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { InlineSvg } from '../../InlineSvg';

interface IMobileSearchProps {
  t: TranslationFunction;
}

export class MobileSearch extends React.PureComponent<IMobileSearchProps> {
  public render() {
    return (
      <li className="shell-menu-mobile-search d-block d-sm-none" id="shell-menu-mobile-search">
        <form>
          <div className="shell-menu-mobile-search-input-wrapper">
            <label>
              <span className="sr-only">{ this.props.t('header:search.input.button') }</span>
              <input placeholder={this.props.t('header:search.input.button')} type="search" />
            </label>
            <button type="button">
              <InlineSvg src="@villemontreal/font-icon/svg-files/071-mini-x-circulaire.svg" />
            </button>
          </div>
        </form>
      </li>
    );
  }
}
