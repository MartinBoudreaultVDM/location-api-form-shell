import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { MenuItem } from './MenuItem';
import { MobileSearch } from './MobileSearch';
import { SubMenuItem } from './SubMenuItem';

interface IMenuProps {
  t: TranslationFunction;
}

export class Menu extends React.PureComponent<IMenuProps> {
  public render() {
    return (
      <div className="shell-menu" id="shell-menu" data-component-type="navigation" data-component-position="gauche" data-component-name="menu">
        <ul>
          <MenuItem text={this.props.t('header:menu.home')} url={this.props.t('header:menu.homeUrl')} />
          <SubMenuItem text={this.props.t('header:menu.news.label')} url={this.props.t('header:menu.news.url')} menuId="shell-menu-news" t={this.props.t}>
            <MenuItem text={this.props.t('header:menu.news.noticesAndAlerts')} url={this.props.t('header:menu.news.noticesAndAlertsUrl')} />
            <MenuItem text={this.props.t('header:menu.news.news')} url={this.props.t('header:menu.news.newsUrl')} />
          </SubMenuItem>
          <MenuItem text={this.props.t('header:menu.requestsAndPayments')} url={this.props.t('header:menu.requestsAndPaymentsUrl')} />
          <SubMenuItem text={this.props.t('header:menu.usefulInformation.label')} url={this.props.t('header:menu.usefulInformation.url')} menuId="shell-menu-useful" t={this.props.t}>
            <MenuItem text={this.props.t('header:menu.usefulInformation.wasteAndRecycling')} url={this.props.t('header:menu.usefulInformation.wasteAndRecyclingUrl')} />
            <MenuItem text={this.props.t('header:menu.usefulInformation.housingAndTaxes')} url={this.props.t('header:menu.usefulInformation.housingAndTaxesUrl')} />
            <MenuItem text={this.props.t('header:menu.usefulInformation.pets')} url={this.props.t('header:menu.usefulInformation.petsUrl')} />
            <MenuItem text={this.props.t('header:menu.usefulInformation.lawAndJustice')} url={this.props.t('header:menu.usefulInformation.lawAndJusticeUrl')} />
          </SubMenuItem>
          <MenuItem text={this.props.t('header:menu.findAPlace')} url={this.props.t('header:menu.findAPlaceUrl')} />
          <MenuItem text={this.props.t('header:menu.contactUs')} url={this.props.t('header:menu.contactUsUrl')} />
          <MobileSearch t={this.props.t} />
          <MenuItem text={this.props.t('header:changeLanguage.fullLabel')} url={this.props.t('header:changeLanguage.defaultUrl')} />
        </ul>
      </div>
    );
  }
}
