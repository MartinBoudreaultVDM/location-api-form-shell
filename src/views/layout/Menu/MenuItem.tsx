import * as classnames from 'classnames';
import * as React from 'react';

interface IMenuItemProps {
  className?: string;
  text: string;
  url: string;
}

export class MenuItem extends React.PureComponent<IMenuItemProps> {
  public render() {
    return (
      <li className={classnames('shell-menu-item', this.props.className)}>
        <a href={this.props.url}>{ this.props.text }</a>
      </li>
    );
  }
}
