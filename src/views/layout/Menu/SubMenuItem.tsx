import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { InlineSvg } from '../../InlineSvg';
import { MenuItem } from './MenuItem';

interface ISubMenuItemProps {
  menuId: string;
  t: TranslationFunction;
  text: string;
  url: string;
}

export class SubMenuItem extends React.PureComponent<ISubMenuItemProps> {
  public render() {
    return [
      <li className="shell-menu-item">
        <button data-menu-id={this.props.menuId}>
          <span>{ this.props.text }</span>
          <InlineSvg src="@villemontreal/font-icon/svg-files/063-fleche-droite.svg" />
        </button>
      </li>,
      <ul id={this.props.menuId} className="shell-sub-menu-item" data-link-section={this.props.text}>
        <li className="shell-menu-item shell-menu-back">
          <button>
            <InlineSvg src="@villemontreal/font-icon/svg-files/065-fleche-gauche.svg" />
            <span>{ this.props.t('header:menu.back') }</span>
          </button>
        </li>
        <MenuItem className="shell-menu-item-header" text={this.props.text} url={this.props.url} />
        { this.props.children }
      </ul>,
    ];
  }
}
