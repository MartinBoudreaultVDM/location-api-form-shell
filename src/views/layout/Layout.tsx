import { TranslationFunction } from 'i18next';
import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';
import { Footer } from './Footer';
import { Head } from './Head';
import { Header } from './Header';
import { Menu } from './Menu';
const { version } = require('../../../package.json');

interface ILayoutProps {
  clientSideData: { [key: string]: string };
  t: TranslationFunction;
  title?: string;
  head?: string;
  bodyAttributes?: { [name: string]: string };
  isAuthenticated: boolean;
}

export class Layout extends React.PureComponent<ILayoutProps> {
  public render() {
    return (
      <html>
        <head dangerouslySetInnerHTML={{ __html: this.mergeHeads() }} />
        <body {...this.getBodyAttribues()}>
          <a className="shell-skip-to-content" href="#shell-main-content">
            {this.props.t('common:skipToMainContent')}
          </a>
          <Menu t={this.props.t} />
          <div className="shell-page">
            <Header t={this.props.t} isAuthenticated={this.props.isAuthenticated} />
            <div className="shell-content" id="shell-main-content">
              {this.props.children}
            </div>
            <Footer t={this.props.t} />
          </div>
          <script src={`/shell/javascripts/main.js?v=${version}`} />
        </body>
      </html>
    );
  }

  private mergeHeads() {
    const headContainsTitle = this.props.head && this.props.head.indexOf('<title>') !== -1;
    const ownHead = ReactDOMServer.renderToString(
      <Head
        t={this.props.t}
        title={this.props.title}
        includeTitle={!headContainsTitle}
        clientSideData={this.props.clientSideData}
      />
    );

    if (this.props.head) {
      return ownHead + this.props.head;
    }

    return ownHead;
  }

  private getBodyAttribues() {
    if (!this.props.bodyAttributes) {
      return {};
    }

    return Object.keys(this.props.bodyAttributes).reduce((acc: any, attr: any) => {
      const reactAttr = this.formatAttributeForJSX(attr);
      acc[reactAttr] = this.props.bodyAttributes![attr];
      return acc;
    }, {});
  }

  private formatAttributeForJSX(attr: string) {
    switch (attr) {
      case 'class':
        return 'className';

      default:
        return attr;
    }
  }
}
