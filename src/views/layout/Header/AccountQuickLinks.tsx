import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { InlineSvg } from '../../InlineSvg';

interface IAccountQuickLinksProps {
  t: TranslationFunction;
}

export class AccountQuickLinks extends React.PureComponent<IAccountQuickLinksProps> {
  public render() {
    return (
      <button id="shell-auth-account" className="shell-header-dropdown">
        <div className="shell-header-dropdown-button">
          <span className="d-none d-lg-inline-block">{this.props.t('header:myAccount.myAccount')}</span>
          <InlineSvg src="@villemontreal/font-icon/svg-files/003-profil.svg" className="shell-header-account" />
        </div>

        <ul className="shell-header-dropdown-menu d-none">
          <li>
            <a href={this.props.t('header:myAccount.dashboardUrl')}>{this.props.t('header:myAccount.dashboard')}</a>
          </li>
          <li>
            <a href={this.props.t('header:myAccount.profileUrl')}>{this.props.t('header:myAccount.profile')}</a>
          </li>
          <li>
            <a href={this.props.t('header:myAccount.accountParametersUrl')}>
              {this.props.t('header:myAccount.accountParameters')}
            </a>
          </li>
          <li>
            <a href={this.props.t('header:myAccount.notificationsUrl')}>
              {this.props.t('header:myAccount.notifications')}
            </a>
          </li>
          <li className="divider" />
          <li>
            <a id="shell-auth-logout">
              {this.props.t('header:myAccount.logout')}
            </a>
          </li>
        </ul>
      </button>
    );
  }
}
