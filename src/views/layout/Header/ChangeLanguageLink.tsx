import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IChangeLanguageLinkProps {
  t: TranslationFunction;
}

export class ChangeLanguageLink extends React.PureComponent<IChangeLanguageLinkProps> {
  public render() {
    return (
      <a href={this.props.t('header:changeLanguage.defaultUrl')} id="shell-change-language-link">
        <span className="sr-only">{ this.props.t('header:changeLanguage.fullLabel') }</span>
        { this.props.t('header:changeLanguage.shortLabel') }
      </a>
    );
  }
}
