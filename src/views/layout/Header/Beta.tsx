import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IBetaProps {
  t: TranslationFunction;
}

// tslint:disable-next-line:max-line-length
const oldHomepage = 'http://ville.montreal.qc.ca/portal/page?_pageid=5798,85041649&amp;_dad=portal&amp;_schema=PORTAL';

export class Beta extends React.PureComponent<IBetaProps> {
  public render() {
    return (
      <div className="shell-beta">
        <div className="shell-beta-inner">
          <div className="shell-beta-inner-left">
            <div>{ this.props.t('header:beta.message') }</div>
            <a className="see-more d-none d-md-inline-block"
              href={this.props.t('header:beta.learnMoreUrl')}
              target="_blank"
              aria-label={this.props.t('header:beta.learnMoreAria')}>
              <span className="sr-only">{ this.props.t('header:beta.learnMore') }</span>
              <span>{ this.props.t('header:beta.learnMore') }</span>
            </a>
          </div>
          <a className="d-none d-md-inline-block"
            href={oldHomepage}
            target="_blank"
            aria-label={this.props.t('header:beta.quitAria')}>
            <span className="sr-only">{ this.props.t('header:beta.quit') }</span>
            <span>{ this.props.t('header:beta.quit') }</span>
          </a>
        </div>
      </div>
    );
  }
}
