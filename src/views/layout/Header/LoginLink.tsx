import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { InlineSvg } from '../../InlineSvg';

interface ILoginLinkProps {
  t: TranslationFunction;
}

export class LoginLink extends React.PureComponent<ILoginLinkProps> {
  public render() {
    return (
      <button id="shell-auth-login">
        <span className="sr-only">{ this.props.t('header:myAccount.loginScreenReader') }</span>
        <span className="d-none d-lg-inline-block">{ this.props.t('header:myAccount.login') }</span>
        <InlineSvg src="@villemontreal/font-icon/svg-files/003-profil.svg" className="shell-header-account" />
      </button>
    );
  }
}
