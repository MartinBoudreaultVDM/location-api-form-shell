import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { MenuButton } from '../Menu';
import { AccountQuickLinks } from './AccountQuickLinks';
import { Beta } from './Beta';
import { ChangeLanguageLink } from './ChangeLanguageLink';
import { LoginLink } from './LoginLink';
import { QuickSearch, QuickSearchButton } from './QuickSearch';

interface IHeaderProps {
  isAuthenticated: boolean;
  t: TranslationFunction;
}

export class Header extends React.PureComponent<IHeaderProps> {
  public render() {
    return (
      <header className="shell-header">
        <Beta t={this.props.t} />
        <nav data-component-type="navigation" data-component-position="entete" data-component-name="entete">
          <MenuButton />
          <a className="shell-header-logo" href={this.props.t('header:homeUrl')}>
            <img alt="Logo Ville de Montréal" src="/shell/images/logo.svg" />
          </a>
          <ul className="shell-header-icons">
            <li className="d-none d-sm-inline-block">
              <QuickSearchButton t={this.props.t} />
            </li>
            <li>
              {this.props.isAuthenticated ? <AccountQuickLinks t={this.props.t} /> : <LoginLink t={this.props.t} />}
            </li>
            <li className="d-none d-lg-block">
              <ChangeLanguageLink t={this.props.t} />
            </li>
          </ul>
        </nav>
        <QuickSearch t={this.props.t} />
      </header>
    );
  }
}
