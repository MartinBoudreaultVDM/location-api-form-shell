import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { InlineSvg } from '../../../InlineSvg';

interface IQuickSearchProps {
  t: TranslationFunction;
}

export class QuickSearch extends React.PureComponent<IQuickSearchProps> {
  public render() {
    return (
      <div className="shell-quick-search" id="shell-quick-search-input">
        <div className="shell-quick-search-inner">
          <form>
            <InlineSvg src="@villemontreal/font-icon/svg-files/002-recherche.svg" className="shell-quick-search-label-icon" />
            <label>
              <span className="sr-only">{ this.props.t('header:search.input.placeholder') }</span>
              <input type="search" placeholder={this.props.t('header:search.input.placeholder')} />
            </label>
            <button>{ this.props.t('header:search.input.button') }</button>
          </form>
        </div>
      </div>
    );
  }
}
