import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { InlineSvg } from '../../../InlineSvg';

interface IQuickSearchButtonProps {
  t: TranslationFunction;
}

export class QuickSearchButton extends React.PureComponent<IQuickSearchButtonProps> {
  public render() {
    return (
      <button id="shell-quick-search-button">
        <span className="sr-only">{this.props.t('header:search.buttonScreenReader')}</span>
        <span className="d-none d-lg-inline-block">{this.props.t('header:search.button')}</span>
        <InlineSvg
          src="@villemontreal/font-icon/svg-files/002-recherche.svg"
          className="shell-header-search d-none d-sm-inline-block"
        />
      </button>
    );
  }
}
