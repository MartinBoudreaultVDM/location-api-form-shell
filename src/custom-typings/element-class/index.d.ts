declare module 'element-class' {
  class ElementClass {
    remove(className: string): void;
    add(className: string): void;
    toggle(className: string): void;
    has(className: string): boolean;
  }

  function elementClass(node: Node): ElementClass;

  namespace elementClass {

  }
  export = elementClass;
}
