import * as elementClass from 'element-class';
import { config } from '../utils';

const activeClass = 'shell-quick-search-active';

export function registerQuickSearchEventHandlers() {
  const quickSearchButton = document.getElementById('shell-quick-search-button');

  if (quickSearchButton) {
    quickSearchButton.addEventListener('click', toggleQuickSearch);
  }
}

function toggleQuickSearch(event: MouseEvent) {
  event.stopPropagation();
  const quickSearchInput = document.getElementById('shell-quick-search-input')!;

  if (elementClass(quickSearchInput).has(activeClass)) {
    hideQuickSearch();
  } else {
    showQuickSearch();
  }
}

function hideQuickSearch() {
  const quickSearchInput = document.getElementById('shell-quick-search-input')!;
  elementClass(quickSearchInput).remove(activeClass);

  quickSearchInput
    .getElementsByTagName('form')
    .item(0)
    .removeEventListener('submit', handleSubmit);
  quickSearchInput.removeEventListener('click', doNothing);
  document.body.removeEventListener('click', hideQuickSearch);
}

function showQuickSearch() {
  const quickSearchInput = document.getElementById('shell-quick-search-input')!;

  elementClass(quickSearchInput).add(activeClass);
  setTimeout(() => {
    // Focus on the input
    quickSearchInput
      .getElementsByTagName('input')
      .item(0)
      .focus();
  }, 100);

  quickSearchInput
    .getElementsByTagName('form')
    .item(0)
    .addEventListener('submit', handleSubmit);
  document.body.addEventListener('click', hideQuickSearch);
  // Get the inner div, is the one with the ID contains the padding on the side
  quickSearchInput.firstChild!.addEventListener('click', doNothing as any);
}

function handleSubmit(event: Event) {
  event.preventDefault();
  const input = document.querySelector('#shell-quick-search-input input') as HTMLInputElement;
  redirectToSearch(input.value);
}

export function redirectToSearch(term: string) {
  if (config.language === 'en') {
    window.location.href = `/en/search?q=${term}`;
  } else {
    window.location.href = `/recherche?q=${term}`;
  }
}

function doNothing(event: MouseEvent) {
  event.stopPropagation();
}
