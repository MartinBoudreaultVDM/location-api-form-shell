import { registerAuthenticationActions } from './authentication';
import { registerMenuEventHandlers } from './menu';
import { registerQuickSearchEventHandlers } from './quick-search';

document.addEventListener(
  'DOMContentLoaded',
  () => {
    registerMenuEventHandlers();
    registerQuickSearchEventHandlers();
    registerAuthenticationActions();
  },
  false
);
