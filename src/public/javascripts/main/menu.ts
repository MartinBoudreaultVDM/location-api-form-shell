import * as elementClass from 'element-class';
import { redirectToSearch } from './quick-search';

const bodyClass = 'shell-menu-opened';

export function registerMenuEventHandlers() {
  registerMenuButtonHandler();
  registerSubMenuButtonHandler();
  registerMobileSearchHandler();
}

function registerMenuButtonHandler() {
  const menuButton = document.getElementById('shell-menu-button');

  if (menuButton) {
    menuButton.addEventListener('click', toggleMenu);
  }
}

function toggleMenu() {
  elementClass(document.body).toggle(bodyClass);
}

function registerSubMenuButtonHandler() {
  const subMenus = document.querySelectorAll('.shell-menu-item > button');

  for (let i = 0; i < subMenus.length; i += 1) {
    const subMenu = subMenus.item(i);
    const menuIdAttribute = subMenu.attributes.getNamedItem('data-menu-id');

    if (menuIdAttribute) {
      // Button to access the sub-menu
      subMenu.addEventListener('click', toggleSubMenu(menuIdAttribute.value));
    } else {
      // Button to go back to the main menu
      subMenu.addEventListener('click', toggleBackSubMenu);
    }
  }
}

function toggleSubMenu(menuId: string) {
  return () => {
    const menuParent = document.getElementById('shell-menu')!;
    const subMenu = document.getElementById(menuId)!;

    // Slide out the current menu to the left
    elementClass(menuParent.firstChild!).add('shell-previous-menu');

    // Slide in the next menu from the right
    elementClass(subMenu).add('shell-next-menu');
    menuParent.appendChild(subMenu);

    setTimeout(() => {
      // Focus on the "Back" button
      const backButton = document.querySelector(`#${menuId} button:first-child`) as any;
      backButton.focus();
    }, 500);
  };
}

function toggleBackSubMenu() {
  const menuParent = document.getElementById('shell-menu')!;
  const mainMenu = menuParent.firstChild!;
  const subMenu = menuParent.children.item(1);

  // Slide in the previous menu from the left
  elementClass(mainMenu).remove('shell-previous-menu');

  setTimeout(() => {
    // Slide out the current menu to the right
    elementClass(subMenu).remove('shell-next-menu');
    mainMenu.appendChild(subMenu);

    // Focus on the previously selected button
    const menuId = subMenu.attributes.getNamedItem('id').value;
    const subMenuButton = document.querySelector(`button[data-menu-id="${menuId}"]`) as any;
    subMenuButton.focus();
  }, 500);
}

function registerMobileSearchHandler() {
  const form = document.querySelector('#shell-menu-mobile-search form')!;
  const clearButton = document.querySelector('#shell-menu-mobile-search button')!;

  form.addEventListener('submit', handleSearchSubmit);
  clearButton.addEventListener('click', clearSearchTerm);
}

function handleSearchSubmit(event: Event) {
  event.preventDefault();
  const input = document.querySelector('#shell-menu-mobile-search input') as HTMLInputElement;
  redirectToSearch(input.value);
}

function clearSearchTerm(event: Event) {
  event.preventDefault();
  const input = document.querySelector('#shell-menu-mobile-search input') as HTMLInputElement;
  input.value = '';
}
