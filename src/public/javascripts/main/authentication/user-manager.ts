import { User, UserManager, WebStorageStateStore } from 'oidc-client';
import { config } from '../../utils';
import { authenticationRepository } from './authentication-repository';

const silentRefreshUrl = `${window.location.origin}/silent-refresh.html`;

export const userManager = new UserManager({
  authority: config.gluu.issuer,
  client_id: config.gluu.clientId,
  response_type: 'id_token token',
  scope: 'openid profile user_name',
  automaticSilentRenew: true,
  silent_redirect_uri: silentRefreshUrl,
  userStore: new WebStorageStateStore({ store: window.localStorage, prefix: 'shell.' })
});

userManager.events.addUserLoaded((user: User) => {
  authenticationRepository.accessToken = user.access_token;
});

// automaticSilentRenew is fired on tokenExpiring, but if it's already expired it doesn't fire automatically
userManager.events.addAccessTokenExpired(async () => {
  await userManager.signinSilent();
});
