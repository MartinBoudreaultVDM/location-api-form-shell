import { config } from '../../utils';
import { userManager } from './user-manager';

export async function redirectToLogout() {
  const user = await userManager.getUser();

  await userManager.signoutRedirect({
    post_logout_redirect_uri: getLogoutRedirectUrl(),
    id_token_hint: user.id_token
  });
}

function getLogoutRedirectUrl() {
  if (config.language === 'en') {
    return `${window.location.origin}/en/logout`;
  }

  return `${window.location.origin}/fr/deconnexion`;
}
