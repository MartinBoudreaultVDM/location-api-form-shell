import * as Cookies from 'js-cookie';

const nonceKey = 'shell_nonce';
const redirectUrlKey = 'shell_redirect_url';
const accessTokenKey = 'access_token';

class AuthenticationRepository {
  public get nonce() {
    return localStorage.getItem(nonceKey) || '';
  }

  public set nonce(value: string) {
    localStorage.setItem(nonceKey, value);
  }

  public removeNonce() {
    localStorage.removeItem(nonceKey);
  }

  public get redirectUrl() {
    return localStorage.getItem(redirectUrlKey) || '';
  }

  public set redirectUrl(value: string) {
    localStorage.setItem(redirectUrlKey, value);
  }

  public removeRedirectUrl() {
    localStorage.removeItem(redirectUrlKey);
  }

  public get accessToken() {
    return Cookies.get(accessTokenKey) || '';
  }

  public set accessToken(value: string) {
    Cookies.set(accessTokenKey, value);
  }

  public removeAccessToken() {
    Cookies.remove(accessTokenKey);
  }
}

export const authenticationRepository = new AuthenticationRepository();
