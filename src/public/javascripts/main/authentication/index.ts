import { hideAccountDropdown, toggleAccountDropdown } from './account-dropdown';
import './handle-gluu-callbacks';
import { redirectToLogin } from './redirect-to-login';
import { redirectToLogout } from './redirect-to-logout';

export function registerAuthenticationActions() {
  const loginButton = document.getElementById('shell-auth-login');
  if (loginButton) {
    loginButton.addEventListener('click', redirectToLogin);
  }

  const accountButton = document.getElementById('shell-auth-account');
  if (accountButton) {
    accountButton.addEventListener('click', toggleAccountDropdown);
    document.body.addEventListener('click', hideAccountDropdown);
  }

  const logoutButton = document.getElementById('shell-auth-logout');
  if (logoutButton) {
    logoutButton.addEventListener('click', redirectToLogout);
  }
}
