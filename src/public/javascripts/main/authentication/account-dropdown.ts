export function toggleAccountDropdown(event: MouseEvent) {
  event.stopPropagation();

  const dropdownMenu = document.querySelector('#shell-auth-account ul')!;
  dropdownMenu.classList.toggle('d-none');
}

export function hideAccountDropdown() {
  const dropdownMenu = document.querySelector('#shell-auth-account ul')!;
  dropdownMenu.classList.add('d-none');
}
