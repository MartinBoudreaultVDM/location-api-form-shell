import * as uuid from 'uuid/v4';
import { config } from '../../utils';
import { authenticationRepository } from './authentication-repository';
import { userManager } from './user-manager';

export async function redirectToLogin() {
  authenticationRepository.nonce = uuid();
  authenticationRepository.redirectUrl = window.location.pathname;

  await userManager.signinRedirect({
    redirect_uri: getLoginRedirectUrl(),
    ui_locales: config.language,
    extraQueryParams: {
      nonce: authenticationRepository.nonce
    }
  });
}

function getLoginRedirectUrl() {
  if (config.language === 'en') {
    return `${window.location.origin}/en/authorize`;
  }

  return `${window.location.origin}/fr/authoriser`;
}
