// tslint:disable:no-floating-promises
import { config } from '../../utils';
import { authenticationRepository } from './authentication-repository';
import { userManager } from './user-manager';

// tslint:disable-next-line:switch-default
switch (window.location.pathname) {
  case '/fr/authoriser':
  case '/en/authorize':
    handleGluuLoginCallback();
    break;

  case '/fr/deconnexion':
  case '/en/logout':
    handleGluuLogoutCallback();
    break;

  case '/silent-refresh.html':
    handleGluuSilentRefreshCallback();
    break;
}

async function handleGluuLoginCallback() {
  await userManager.signinRedirectCallback();
  authenticationRepository.removeNonce();
  redirectToOriginalPage();
}

function redirectToOriginalPage() {
  const redirectUrl = authenticationRepository.redirectUrl;
  authenticationRepository.removeRedirectUrl();
  window.location.href = redirectUrl;
}

async function handleGluuLogoutCallback() {
  await userManager.signoutRedirectCallback();
  authenticationRepository.removeAccessToken();
  redirectToHomePage();
}

function redirectToHomePage() {
  if (config.language === 'en') {
    window.location.href = '/en/';
  } else {
    window.location.href = '/';
  }
}

async function handleGluuSilentRefreshCallback() {
  await userManager.signinSilentCallback();
}
