const config = (window as any).shellConfig;

export const language: 'fr' | 'en' = config.language;
export const gluu: IGluuConfig = config.gluu;

interface IGluuConfig {
  issuer: string;
  clientId: string;
}
