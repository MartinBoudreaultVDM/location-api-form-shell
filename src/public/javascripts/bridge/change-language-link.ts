export function changeLanguageLink(url: string) {
  const changeLanguageHyperlink = document.getElementById('shell-change-language-link')!;
  changeLanguageHyperlink.setAttribute('href', url);
}
