import { changeLanguageLink } from './change-language-link';

const _vdm: any = ((window as any)._vdm = {});
_vdm.sendMessage = sendMessage;

function sendMessage(message: string, data: any) {
  const dataOrDefault = data || {};

  switch (message) {
    case 'change-language-link':
      changeLanguageLink(dataOrDefault.url);
      break;

    default:
      // tslint:disable-next-line:no-console
      console.warn(`Unknown message: ${message}`);
  }
}
