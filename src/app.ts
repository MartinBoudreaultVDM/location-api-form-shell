import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import * as logger from 'morgan';
import { initializeAuthentication } from './authentication';
import { initializeI18n, registerErrorHandler, servePublic, viewEngine } from './initialization';
import { router } from './routes';

export const app = express();

try {
  initializeApp().catch(handleError);
} catch (err) {
  handleError(err);
}

async function initializeApp() {
  viewEngine(app);
  servePublic(app);
  app.use(logger('dev'));
  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  initializeI18n(app);
  initializeAuthentication(app);
  app.use(router);
  registerErrorHandler(app);
}

function handleError(error: Error) {
  // tslint:disable-next-line:no-console
  console.error(error);
}
