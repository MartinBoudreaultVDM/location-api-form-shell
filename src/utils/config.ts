// By default config uses development when no NODE_ENV is set, but we need different configs locally and in dev
// If nothing is set, we set it to 'localhost' before clearing it back to it's initial value
let deleteNodeEnv = false;

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'localhost';
  deleteNodeEnv = true;
}

import * as nodeConfig from 'config';

if (deleteNodeEnv) {
  delete process.env.NODE_ENV;
}

export const config = {
  domain: nodeConfig.get('domain') as string,
  gluu: {
    issuer: nodeConfig.get('gluu.issuer') as string,
    clientId: nodeConfig.get('gluu.clientId') as string
  }
};
