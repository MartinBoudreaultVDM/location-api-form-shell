import * as fs from 'fs';
import * as path from 'path';

export function readModuleFileSync(filePath: string): string {
  const fullPath = path.join(__dirname, '..', '..', 'node_modules', filePath);
  return fs.readFileSync(fullPath, 'utf8');
}
