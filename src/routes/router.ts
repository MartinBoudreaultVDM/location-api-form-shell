import * as express from 'express';
import * as path from 'path';
import { createAppMiddleware } from './app-middleware';
import { routes } from './config';

const router = express.Router();
router.get('/autocomplete', (req, res, next) => {
  const options = {
    root: path.join(__dirname, '../..', 'autocomplete'),
    headers: {
      'x-timestamp': Date.now(),
      'x-sent': true
    }
  };

  const fileName = 'demo.html';
  res.sendFile(fileName, options, err => {
    if (err) {
      next(err);
    }
  });
});
routes.forEach(route => {
  const middleware = createAppMiddleware(route);
  router.all(route.routes.fr, middleware);

  if (route.routes.en) {
    router.all(route.routes.en);
  }
});

export { router };
