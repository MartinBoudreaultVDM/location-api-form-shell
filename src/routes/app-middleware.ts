import axios, { AxiosPromise, AxiosRequestConfig, AxiosResponse } from 'axios';
import * as cheerio from 'cheerio';
import { Response } from 'express';
import { asyncMiddleware, logger } from '../utils';
import { IRouteInterface, IRouteSource } from './config';

export function createAppMiddleware(route: IRouteInterface) {
  return asyncMiddleware(async (req, res, next) => {
    logger.info(`${req.url} routed to ${route.name}`);
    const response = await getPage(req.url, req.method, route.source);
    handleResponse(response, res);
  });
}

function getPage(url: string, method: string, source: IRouteSource): AxiosPromise {
  const config: AxiosRequestConfig = {
    method,
    baseURL: source.url,
    responseType: 'arraybuffer'
  };

  if (source.basicAuth) {
    config.auth = source.basicAuth;
  }

  return axios(url, config);
}

function handleResponse(response: AxiosResponse, res: Response) {
  if (response.headers['content-type'].startsWith('text/html')) {
    wrapHtmlInShell(res, response.data);
  } else {
    mapHeaders(res, response.headers);
    res.send(response.data);
  }
}

function wrapHtmlInShell(res: Response, html: Buffer) {
  const decodedHtml = html.toString('utf-8');
  const $ = cheerio.load(decodedHtml);

  res.render('ExternalPage', {
    head: $('head').html(),
    body: $('body').html(),
    bodyAttributes: $('body').attr()
  });
}

function mapHeaders(res: Response, headers: { [name: string]: string }) {
  for (const header of Object.keys(headers)) {
    res.setHeader(header, headers[header]);
  }
}
