import { IRouteInterface } from './route-interface';

export const routes: IRouteInterface[] = [
  {
    name: 'Permis animal',
    source: {
      url: 'http://localhost:4200/'
    },
    routes: {
      fr: '/permis-animalier-citoyen/*'
    }
  },
  // This one must be last since it matches all routes
  {
    name: 'Portail',
    source: {
      url: 'http://localhost:4001/'
    },
    routes: {
      fr: '*'
    }
  }
];
