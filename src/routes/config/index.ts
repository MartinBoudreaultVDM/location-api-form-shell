import { IRouteInterface } from './route-interface';
import { routes as acceptRoutes } from './routes.accept';
import { routes as devRoutes } from './routes.dev';
import { routes as localRoutes } from './routes.local';

let routes: IRouteInterface[];

switch (process.env.NODE_ENV) {
  case 'development':
    routes = devRoutes;
    break;

  case 'acceptation':
    routes = acceptRoutes;
    break;

  default:
    routes = localRoutes;
}

export { IRouteInterface, IRouteSource } from './route-interface';

export { routes };
