export interface IRouteInterface {
  name: string;
  source: IRouteSource;
  routes: {
    fr: string;
    en?: string;
  };
}

export interface IRouteSource {
  url: string;
  basicAuth?: {
    username: string;
    password: string;
  };
}
