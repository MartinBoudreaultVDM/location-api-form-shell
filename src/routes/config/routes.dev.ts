import { IRouteInterface } from './route-interface';

export const routes: IRouteInterface[] = [
  {
    name: 'Permis animal',
    source: {
      url: 'https://services.dev.interne.montreal.ca/'
    },
    routes: {
      fr: '/permis-animalier-citoyen/*'
    }
  },
  // This one must be last since it matches all routes
  {
    name: 'Portail',
    source: {
      url: 'https://dev.montreal.ca/',
      basicAuth: {
        username: 'mtldev',
        password: 'T5nzA67d'
      }
    },
    routes: {
      fr: '*'
    }
  }
];
