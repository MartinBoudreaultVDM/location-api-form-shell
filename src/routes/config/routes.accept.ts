import { IRouteInterface } from './route-interface';

export const routes: IRouteInterface[] = [
  {
    name: 'Permis animal',
    source: {
      url: 'https://services.dev.interne.montreal.ca/'
    },
    routes: {
      fr: '/permis-animalier-citoyen/*'
    }
  },
  // This one must be last since it matches all routes
  {
    name: 'Portail',
    source: {
      url: 'https://accept.montreal.ca/',
      basicAuth: {
        username: 'mtlacc',
        password: 'T5nzD67a'
      }
    },
    routes: {
      fr: '*'
    }
  }
];
