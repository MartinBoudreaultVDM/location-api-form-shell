#!groovy
@Library('cicd-lib@develop')
import vdmtl.cicd.*
import jenkins.model.*


pipeline = new Pipeline()

    //TODO v2: add different contexts for deployment, according to the branch
    // Add description (bilingue)
ctx = pipeline.createContext([
    namespace: ["sn", "shell-portal"],
    application: [
        name: "shell-portal-web",
        tier: "frontend",
        runtime: "nodejs", // Available Platforms: nodejs, drupal
        framework: "nodejs",
        description: "shell-portal-web",
        icon: "https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/svgs/fi-lightbulb.svg",
        keywords: ["shell", "portal", "web"],
    ],
    packaging: [
        dockerfilePath: "./Dockerfile",
        helmChartName: "web-api"
    ],
    execution: [
        service: [
            port: 4000
        ],
        resources: [
            minCpu: "100m",
            minMemory: "100Mi",
            maxCpu: "1000m",
            maxMemory: "1000Mi"
        ],
        autoScaling: [
            minReplicas: 1,
            maxReplicas: 3,
            cpuUsagePercentage: 50
        ],
        probes: [
            enabled: false,
            liveness: "/permis-animalier-citoyen/assets/images/logo.svg",
            readiness: "/permis-animalier-citoyen/assets/images/logo.svg",
        ],
        monitoring: [
            enabled: false,
            path: "/permis-animalier-citoyen/assets/images/logo.svg"
        ],
    ],
    ingress : [
        dev: [
            [
                uri : "beta.kube.dev.ile.montreal.qc.ca",
                paths : ["/"]
            ],
            [
                uri : "beta.dev.interne.montreal.ca",
                paths : ["/"]
            ]
        ],
        accept: [
            [
                uri : "beta.kube.accept.ile.montreal.qc.ca",
                paths : ["/"]
            ],
            [
                uri : "beta.accept.interne.montreal.ca",
                paths : ["/"]
            ]
        ],
        prod: [
            [
                uri : "beta.kube.ile.montreal.qc.ca",
                paths : ["/"]
            ],
            [
                uri : "beta.montreal.ca",
                paths : ["/"]
            ]
        ],

    ],
    approvers: [
        // List of AD groups or user codes
        "accept": [
            //"udev123",
            "G-UNIX-Jenkins-Admin"
        ],
        "prod": [
            "G-UNIX-Jenkins-Admin"
        ],
    ],
    notifications: [
        mail: [
            to: [
                "chdallaire"
            ]
        ]
    ],
    debug: true
])


pipeline.start(ctx) {

    pipeline.withSourceCode(ctx) {

        pipeline.checkoutStage(ctx) {
            checkout scm;
        }

        pipeline.buildStage(ctx) {
            pipeline.buildDockerImage(ctx)
        }

        pipeline.prePublishStage(ctx) {
            pipeline.publishDraftDockerImage(ctx)
        }
    }

    pipeline.withDraftDockerImage(ctx) {
        pipeline.testInDraftDockerContainerStage(ctx) {
            try {
                sh """
                    export JUNIT_REPORT_PATH=${env.WORKSPACE}/output/test-results/report.xml
                    cd /usr/src/app
                    yarn lint
                """
            } finally {
                //sh "mv /usr/src/app/output/test-results/report.xml ."
                try {
                    junit "output/test-results/report.xml"
                } catch(e){}
            }
        }

        pipeline.publishStage(ctx) {
            pipeline.publishDockerImage(ctx)
        }

        pipeline.deployStage(ctx) {
            pipeline.deployApp(ctx)
        }
    }
}

