const { CheckerPlugin } = require('awesome-typescript-loader');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const outputPath = path.join(__dirname, 'dist', 'public');

module.exports = {
  devtool: 'inline-source-map',

  entry: {
    'javascripts/main': path.join(__dirname, 'src/public/javascripts/main/index.ts'),
    'javascripts/bridge': path.join(__dirname, 'src/public/javascripts/bridge/index.ts'),
    'stylesheets/style': path.join(__dirname, 'src/public/stylesheets/style.scss')
  },

  output: {
    filename: '[name].js',
    path: path.join(outputPath)
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'awesome-typescript-loader',
            options: {
              configFileName: path.join(__dirname, 'src', 'public', 'javascripts', 'tsconfig.json')
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.(png|gif|jpg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            }
          }
        ]
      }
    ]
  },

  plugins: [
    new CheckerPlugin(),
    new CopyWebpackPlugin([
      {
        context: 'src/public',        
        from: 'favicon.png'
      },
      {
        context: 'src/public/images',
        from: '**/*.@(png|gif|jpg|svg)',
        to: 'images'
      }
    ]),
    new ExtractTextPlugin({
      filename: '[name].css'
    })
  ]
};
