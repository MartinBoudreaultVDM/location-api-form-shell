FROM node:8.0
MAINTAINER VdMtl
ARG ENV=dev
ARG GIT_COMMIT=unknown

# GIT label of the packaged code
LABEL GIT_COMMIT=${GIT_COMMIT}

# Create app directory
RUN mkdir -p /usr/src/app
RUN echo "America/Montreal" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

WORKDIR /usr/src/app

# Install all dependencies
COPY . /usr/src/app
RUN npm install -g yarn@1.3.2 && \
    yarn

# Bundle app source
RUN yarn build

# Lanch server
EXPOSE 4000
CMD ["yarn", "start"]
